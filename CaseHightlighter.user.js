// ==UserScript==
// @name         Case Highlighter
// @namespace    https://na53.salesforce.com/
// @version      0.1
// @description  Highlight important cases
// @author       Josiah Zimmermann
// @match        https://na53.salesforce.com/*
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_deleteValue
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// ==/UserScript==

(function() {
    'use strict';
    var i = setInterval(function() {
        if(document.getElementById('colorPicker') == null && document.getElementsByClassName('controls')[0] != null) {
            var controlPanel = document.getElementsByClassName('controls')[0];
            var colorbtn = `
                    <label for='yellow'>Yellow</label><input type='radio' name='color' value='1' checked />
                    <label for='red'>Red</label><input type='radio' name='color' value='2'/>
                    <label for='green'>Green</label><input type='radio' name='color' value='3'/>
                    <label for='blue'>Blue</label><input type='radio' name='color' value='4'/>`;
            var form = document.createElement('form');
            form.id = "colorPicker";
            form.innerHTML = colorbtn;
            controlPanel.appendChild(form);
        }

        $('table.x-grid3-row-table>tbody>tr').each(function(e, obj) {
            var caseNumber = $(obj).find('td>div.x-grid3-col-CASES_CASE_NUMBER').text();
            var actionCol = $(obj).find('td>div.x-grid3-col-ACTION_COLUMN');
            var button = $(actionCol).find('button');
            var caseState = GM_getValue(caseNumber);
            var btn_add = "<button type='button'>+</button>";
            var btn_del = "<button type='button'>-</button>";
            button.css("font-size","10px");

            switch(caseState) {
                case "1":
                    $(obj).css("background-color", "yellow");
                    break;
                case "2":
                    $(obj).css("background-color", "red");
                    break;
                case "3":
                    $(obj).css("background-color", "green");
                    break;
                case "4":
                    $(obj).css("background-color", "blue");
                    break;
                default:
                    $(obj).css("background-color", "white");
            }

            if(caseState == null) {
                $(button).click(function(){
                    var colors = document.getElementById('colorPicker').children;
                    var highlightColor = 1;
                    for (var i=0, len=colors.length; i<len; i++) {
                        if( colors[i].checked) {
                            highlightColor = colors[i].value;
                            break;
                        }
                    }
                    GM_setValue(caseNumber,highlightColor);
                    $(button).remove();
                });
                if ($(button).text() != "+") {
                    $(actionCol).prepend(btn_add);
                }
            } else {
                $(button).click(function(){
                    GM_deleteValue(caseNumber)
                    $(button).remove();
                });
                if ($(button).text() != "-") {
                    $(actionCol).prepend(btn_del);
                }
            }
        });
    }, 4000);
})();